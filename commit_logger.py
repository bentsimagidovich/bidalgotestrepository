#!/usr/bin/env python
# -*- coding: utf-8 -*-
from MySQLdb import connect, OperationalError
from enum import Enum
import os, logging
from subprocess import check_output

logger = logging.getLogger(__name__)


class ConnectionType(Enum):
    generic = "generic"
    db = "db"


class DBConnector(object):

    def __init__(self, user, passwd):
        self.db_name = "git"
        self.table_name = "commits"
        self.user = user
        self.passwd = passwd
        self.conn = {
            ConnectionType.generic: None,
            ConnectionType.db: None
        }
        self.connect_all_and_init_db()

    def connect_all_and_init_db(self):
        logger.info("Connecting to Databases...")
        self.conn[ConnectionType.generic] = connect(host="localhost", user=self.user, passwd=self.passwd)
        self.check_db_exists()
        self.conn[ConnectionType.db] = connect(host="localhost", user=self.user, passwd=self.passwd, db=self.db_name)
        self.check_if_table_exists()
        logger.info("Connected and initialized.")

    def run_sql(self, query, conn_type=ConnectionType.db, get_output=True):
        with self.conn[conn_type] as cursor:
            logger.debug("executing '%s'" % query)
            cursor.execute(query)
            self.conn[conn_type].commit()
            if get_output:
                return cursor.fetchall()
            else:
                return cursor

    def check_db_exists(self):
        result = self.run_sql(query="SHOW DATABASES", conn_type=ConnectionType.generic)
        databases = [i[0] for i in result]  # normalazing (('sys',)...)
        if self.db_name not in databases:
            logger.warning("Database '%s' doesn't exist. Going to create" % self.db_name)
            self.run_sql(query="CREATE DATABASE " + self.db_name, conn_type=ConnectionType.generic)

    def check_if_table_exists(self):
        result = self.run_sql(query="SHOW TABLES", conn_type=ConnectionType.db)
        tables = [i[0] for i in result]  # normalazing (('sys',)...)
        if self.table_name not in tables:
            logger.warning("Table '%s' doesn't exist. Going to create" % self.table_name)
            # – commit_id, commit_time, commit_message, commit_user
            sql = "CREATE TABLE %s (commit_id CHAR(40), commit_time INT, commit_message VARCHAR(240), commit_user VARCHAR(100), CONSTRAINT PK_commit_id PRIMARY KEY(commit_id))" % self.table_name
            self.run_sql(query=sql, conn_type=ConnectionType.db)

    def add_commit(self, commit_id, commit_time, commit_message, commit_user):
        table_name = self.table_name
        sql = "INSERT INTO {table_name} (commit_id, commit_time , commit_message , commit_user)"\
              " VALUES ( '{commit_id}', '{commit_time}', '{commit_message}', '{commit_user}')".format(**locals())
        self.run_sql(query=sql, conn_type=ConnectionType.db, get_output=False)

    def get_commit_hashes(self):
        out = self.run_sql(query="SELECT commit_id from commits", conn_type=ConnectionType.db, get_output=True)
        return [i[0] for i in out]


def prettify(s):
    return s.strip()[1:-1]


def get_formatted_commits(branch="origin/master"):
    # used %f for simplicity
    cwd = os.path.dirname(os.path.realpath(__file__))
    out = check_output(["bash", "-ce", 'cd {cwd} && git log {branch} --format=\'"%H", "%ct", "%f", "%an"\''.format(**locals())])
    lines = out.split("\n")
    for line in lines:
        if line:
	    commit_id, commit_time, commit_message, commit_user = line.split(",")
            yield dict(
                commit_id=prettify(commit_id),
                commit_time=prettify(commit_time),
                commit_message=prettify(commit_message),
                commit_user=prettify(commit_user)
            )


def main():
    dbconn = DBConnector(user="root", passwd="root1")
    hashes = dbconn.get_commit_hashes()
    for commit in get_formatted_commits():
        if commit["commit_id"] not in hashes:
            dbconn.add_commit(**commit)


if __name__ == "__main__":
    main()
